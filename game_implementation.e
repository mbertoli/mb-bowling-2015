note
	description: "Summary description for {GAME_IMPLEMENTATION}."
	author: "Mattia Monga"

class
	GAME_IMPLEMENTATION

inherit

	GAME

feature

	score: INTEGER
		do
			Result := points
		end

	roll (pins: INTEGER)
		do
			to_add:=pins
			current_roll := current_roll + 1
			frame_count:=frame_count+1
			if strike > 0 then
				if current_roll<=19 then
					to_add:=(n+1)*pins
					n:=nn
					nn:=0
				end
				strike:=strike-1
			elseif spare=1 then
				if current_roll=20 then
					current_roll:=current_roll-1
					end
				to_add:=2*pins
				spare:=0
			end
			points:=points+to_add
			if pins=10 then
				if current_roll>=19 then
					if last_frame <1 then
						current_roll:=current_roll-1
						last_frame:=last_frame + 1
					end
				else
						current_roll:= current_roll +1
						nn:=nn+1
						n:=n+1
				end
				strike:= strike + 2
			end
			if frame_count=2 then
				if old_pin+pins=10 then
					spare:=1
				end
				frame_count:=0
			end
			old_pin:=pins

		end

	ended: BOOLEAN
		do
			if current_roll = 20 then
				Result := True
			end
		end

feature {NONE}

	to_add: INTEGER
	current_roll: INTEGER
	points: INTEGER
	old_pin: INTEGER
	spare: INTEGER
	strike: INTEGER
	frame_count: INTEGER
	last_frame: INTEGER
	n: INTEGER
	nn: INTEGER

invariant
	valid_roll: 0 <= current_roll and current_roll <= 20

end
